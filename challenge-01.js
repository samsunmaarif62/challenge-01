const readline = require('readline');

const interface = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

function input(question){
    return new Promise(resolve =>{
        interface.question(question, data=>{
            return resolve(data);
        });
    });
}

function tambah(angka1,angka2){
    return angka1+angka2;
}

function kurang(angka1,angka2){
    return angka1-angka2;
}

function bagi(angka1,angka2){
    return angka1/angka2;
}
function kali(angka1,angka2){
    return angka1*angka2;
    
}
function akarKuadrat(angka1){
    return Math.sqrt(angka1);
    
}
function luasPersegi(sisi){
    return sisi*sisi;
    
}
function volumeKubus(sisi){
    return sisi*sisi*sisi;
    
}
function volumeTabung(jarijari,tinggi){
    const phi = 3.14
    return phi*jarijari**2*tinggi;
}

function menu(){
    console.log("Pilih menu rumus : ");
    console.log("Ketik 1 untuk tambah");
    console.log("Ketik 2 untuk kurang");
    console.log("Ketik 3 untuk kali");
    console.log("Ketik 4 untuk bagi");
    console.log("Ketik 5 untuk akar kuadrat");
    console.log("Ketik 6 untuk luas persegi");
    console.log("Ketik 7 untuk volume kubus");
    console.log("Ketik 8 untuk volume Tabung");
}
async function main(){
    try{
        menu()
        let pilihMenu = await input("Masukkan pilihan : ");
        console.log("")
        let inputAngka1;
        let inputAngka2;
        let hasilKalkulasi;
        switch(pilihMenu){
            case "1":
                inputAngka1 = await input("Masukkan angka 1 : ");
                inputAngka2 = await input("Masukkan angka 2 : ");
                hasilKalkulasi = tambah(+inputAngka1,+inputAngka2);
                console.log(`Hasil ${inputAngka1} + ${inputAngka2} adalah ${hasilKalkulasi}`);
                break;
            case "2":
                inputAngka1 = await input("Masukkan angka 1 : ");
                inputAngka2 = await input("Masukkan angka 2 : ");
                hasilKalkulasi = kurang(+inputAngka1,+inputAngka2);
                console.log(`Hasil ${inputAngka1} - ${inputAngka2} adalah ${hasilKalkulasi}`);
                break;
            case "3":
                inputAngka1 = await input("Masukkan angka 1 : ");
                inputAngka2 = await input("Masukkan angka 2 : ");
                hasilKalkulasi = kali(+inputAngka1,+inputAngka2);
                console.log(`Hasil ${inputAngka1} x ${inputAngka2} adalah ${hasilKalkulasi}`);
                break;
            case "4":
                inputAngka1 = await input("Masukkan angka 1 : ");
                inputAngka2 = await input("Masukkan angka 2 : ");
                hasilKalkulasi = bagi(+inputAngka1,+inputAngka2);
                console.log(`Hasil ${inputAngka1} : ${inputAngka2} adalah ${hasilKalkulasi}`);
                break;
            case "5":
                inputAngka1 = await input("Masukkan angka : ");
                hasilKalkulasi = akarKuadrat(+inputAngka1);
                console.log(`Hasil akar kuadrat ${inputAngka1} adalah ${hasilKalkulasi}`);
                break;
            case "6":
                inputAngka1 = await input("Masukkan sisi : ");
                hasilKalkulasi = luasPersegi(+inputAngka1);
                console.log(`Hasil luas perseginya adalah ${hasilKalkulasi}`);
                break;
            case "7":
                inputAngka1 = await input("Masukkan sisi : ");
                hasilKalkulasi = volumeKubus(+inputAngka1);
                console.log(`Hasil volume kubusnya adalah ${hasilKalkulasi}`);
                break;
            case "8":
                inputAngka1 = await input("Masukkan jari-jari : ");
                inputAngka2 = await input("Masukkan tinggi    : ");
                hasilKalkulasi = volumeTabung(+inputAngka1,+inputAngka2);
                console.log(`Hasil volume kubusnya adalah ${hasilKalkulasi}`);
                break;
            default:
                console.log("Menu yang dipilih tidak ada");
                break;
        }
        interface.close();
    }catch(err){
        console.log(err);
    }
}
main()